﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Question : MonoBehaviour
{
    public string questionText;
    public int CorrectIndex;
    public string[] answers = new string[4];
    }