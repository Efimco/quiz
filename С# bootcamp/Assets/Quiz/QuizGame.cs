﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class QuizGame : MonoBehaviour
{
    public Question[] _questions;
    private Button[] _buttons;
    public int Score = 0;
    public string ActiveQuestion;
    public int ActiveQuestionIndex = 0;
  //  public GameObject resultPanel ;

    private void Start()
    {
        _questions = FindObjectsOfType<Question>();

        _buttons = FindObjectsOfType<Button>();
        Array.Reverse(_buttons);

        for (int i = 0; i < _buttons.Length; i++)
        {
            _buttons[i].onClick.RemoveAllListeners();
            var i1 = i;
            _buttons[i].onClick.AddListener(() => LoadNextQuestion(i1));
        }

        ActiveQuestion = _questions[ActiveQuestionIndex].questionText;
    }

    public void LoadNextQuestion(int index)
    {
        if (index == _questions[ActiveQuestionIndex].CorrectIndex)
        {
            if (ActiveQuestionIndex < _questions.Length)
            {
                Score++;
            }
        }

        if (ActiveQuestionIndex < _questions.Length - 1)
        {
            ActiveQuestionIndex++;
        }

        if (ActiveQuestionIndex == _questions.Length - 1)
        {
      //      resultPanel.SetActive(true);
        }


        ActiveQuestion = _questions[ActiveQuestionIndex].questionText;
    }
}