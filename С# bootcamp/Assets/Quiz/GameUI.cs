﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    private TextMeshProUGUI[] Texts;
    private QuizGame _game;

    void Awake()
    {
        Texts = gameObject.GetComponentsInChildren<TextMeshProUGUI>();
        _game = FindObjectOfType<QuizGame>();
    }

    void Update()
    {
        Texts[0].text = _game.ActiveQuestion;
        for (int i = 1; i < Texts.Length; i++)
        {
            Texts[i].text = _game._questions[_game.ActiveQuestionIndex].answers[i - 1];
        }
    }
}